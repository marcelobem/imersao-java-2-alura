# IMERSÃO JAVA 2 - PROJETO PESSOAL

Eis a minha versão do projeto desenvolvido durante a Imersão Java 2 Alura. 

***

## Conteúdo

***

**Aula 01** - Consumindo uma API de filmes com Java  
**Aula 02** - Gerando figurinhas para WhatsApp  
**Aula 03** - Ligando as pontas, refatoração e orientação a objetos  
**Aula 04** - Criando nossa própria API com Spring  
**Aula 05** - Publicando nossa API no Cloud  

### ✅ **Aula 01 - Consumindo uma API de filmes com Java**

**Resultado da Aula**

![Resultado Aula 1](resultados/aula-1-1.png)

### ✅ **Aula 02 - Gerando figurinhas para WhatsApp**

**Resultado da Aula**

![Resultado Aula 2](resultados/aula-2-1.png)