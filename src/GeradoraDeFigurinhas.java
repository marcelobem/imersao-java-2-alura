import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.InputStream;

import javax.imageio.ImageIO;

public class GeradoraDeFigurinhas {
    
    public void cria(InputStream inputStream, String nomeArquivo, String texto) throws Exception {

        // leitura da imagem
        BufferedImage imagemOriginal = ImageIO.read(inputStream);
        
        // cria uma nova imagem com transparencia e tamanho novo (em memória)
        int largura = imagemOriginal.getWidth();
        int altura = imagemOriginal.getHeight();
        int novaAltura = altura + (altura/6);
        BufferedImage novaImagem = new BufferedImage(largura, novaAltura, BufferedImage.TRANSLUCENT);

        // copiar imagem original pro nova imagem (em memória)
        Graphics2D graphics = (Graphics2D) novaImagem.getGraphics();
        graphics.drawImage(imagemOriginal, 0, 0, null);

        // formatar fonte
        var fonte = new Font(Font.SANS_SERIF, Font.BOLD, 200);
        graphics.setFont(fonte);
        graphics.setColor(Color.GREEN);
        // escrever algo em baixo da imagem (em memória)
        graphics.drawString(texto, 0, novaAltura - 180);

        // salvar a imagem em um arquivo
        //create a new string with the strings ",:,' filtered out using regex
        String nomeArquivoSemCaracteresEspeciais = nomeArquivo.replaceAll("[^a-zA-Z0-9.-]", "_");
        
        ImageIO.write(novaImagem, "png", new File("saida/"+nomeArquivoSemCaracteresEspeciais));
    }

}

