import java.io.BufferedReader; import java.io.InputStreamReader; import java.net.HttpURLConnection; import java.net.URL; 
public class GPTChat { 
    public static void main(String[] args) throws Exception { 
        // Create a URL for the chatGPT API endpoint 
        String url = "https://chatgpt-api-v2-staging.azurewebsites.net/query"; 
        // Create the query string to send to the API endpoint, in this case it's a simple math problem: "What is 2 + 2?" 
        String queryString = "{\"question\":\"What is 2 + 2?\", \"top\":1}"; 
        // Create a new connection to the API endpoint using HttpURLConnection 
        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection(); 
        // Set request method and headers for the connection 
        connection.setRequestMethod("POST"); connection.setRequestProperty("Content-Type", "application/json"); 
        // Send queryString as body of POST request 
        connection.getOutputStream().write(queryString .getBytes()); 
        // Read response from server and store in BufferedReader 
        BufferedReader reader = new BufferedReader(new InputStreamReader(connection .getInputStream())); 
        // Print out response from server 
        System.out.println("Response from ChatGPT API:"); System.out .println(reader); reader .close(); 
    } 
}

// //importing necessary libraries 
// import java.io.BufferedReader;
// import java.io.InputStreamReader;
// import java.net.HttpURLConnection;
// import java.net.URL;

// public class GPTChat {

//     public static void main(String[] args) throws Exception {

//         String url = "https://api-chatgpt3-staging-uswest2-prod-shared.azurewebsites.net/query?question=What%20is%204+4"; // API
//                                                                                                                           // URL
//                                                                                                                           // for
//                                                                                                                           // chatGPT

//         URL obj = new URL(url); // creating object of the API url given

//         HttpURLConnection con = (HttpURLConnection) obj.openConnection(); // opening connection with the API url given

//         con.setRequestMethod("GET"); // setting request method to GET

//         int responseCode = con.getResponseCode(); // getting response code from the API request made above

//         System.out.println("Response Code : " + responseCode); // printing out the response code from the GET request
//                                                                // made above

//         BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream())); // creating a buffer reader
//                                                                                              // object to read data from
//                                                                                              // the API request made
//                                                                                              // above

//         String inputLine;
//         StringBuffer response = new StringBuffer();

//         while ((inputLine = in.readLine()) != null) {
//             response.append(inputLine);
//         }
//         in.close();
//         System.out.println("The answer is: " + response);
//     }
// }