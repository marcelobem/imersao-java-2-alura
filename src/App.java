import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

public class App {

    public static Properties getProps() throws IOException {
        InputStream input = new FileInputStream("./app.properties");
        Properties props = new Properties();
        props.load(input);
        return props;
    }

    public static void main(String[] args) throws Exception {

        Properties props = getProps();
        String imdbApiKey = props.getProperty("IMDB_API_KEY");
        String chatGptApiKey = props.getProperty("CHAT_GPT_KEY");
        String nasaApiKey = props.getProperty("NASA_KEY");

        

        String urlImdb = "https://raw.githubusercontent.com/alura-cursos/imersao-java-2-api/main/TopMovies.json?key=" + imdbApiKey;
        ExtratorDeConteudo extratorImdb = new ExtratorDeConteudoDoIMDB();
        
        var urlNasa = "https://api.nasa.gov/planetary/apod?api_key="+nasaApiKey+"&start_date=2023-03-30";
        ExtratorDeConteudo extratorNasa = new ExtratorDeConteudoDaNasa();

        var http = new ClienteHttp();
        String jsonImdb = http.buscaDados(urlImdb);    
        String jsonNasa = http.buscaDados(urlNasa);    

        var imagensDaNasa = extratorNasa.extraiConteudos(jsonNasa);
        var imagensDoImdb = extratorImdb.extraiConteudos(jsonImdb);
        var geradora = new GeradoraDeFigurinhas();

        for(Conteudo conteudo : imagensDaNasa) {
            InputStream inputStream = new URL(conteudo.getUrlImagem()).openStream();
            geradora.cria(inputStream, conteudo.getTitulo()+".png", "Nasa!!");
        }

        for(Conteudo conteudo : imagensDoImdb) {
            InputStream inputStream = new URL(conteudo.getUrlImagem()).openStream();
            geradora.cria(inputStream, conteudo.getTitulo()+".png", "IMDB!!");
        }

        

        // extrair só os dados que interessam: título, poster, rating;
        // manipular e exibir os dados;
        //for (Map<String, String> conteudo : listadeConteudos) {

            //String jsonInputString = "{\"model\": \"text-davinci-003\", \"prompt\": \"Create a funny alterntive title for the movie '"+conteudo.get("fullTitle")+"' is:\", \"max_tokens\": 900, \"temperature\": 0.5, \"frequency_penalty\": 0.8}";
            
            //var res = ApiConsumer.consumerPostWithBearer("https://api.openai.com/v1/completions", chatGptApiKey, jsonInputString);
            //var botTitle = res.get(0).get("text");

            

            //System.out.println("\u001b[1mTítulo Original:\u001b[m " + conteudo.get("title"));
            //System.out.println("\u001b[1mTítulo criado pela AI:\u001b[m " + botTitle);
           // System.out.println("\u001b[1mURL:\u001b[m " + conteudo.get("image"));
            //System.out.println("\u001b[1mRating:\u001b[m " + conteudo.get("imDbRating"));
            // double classificacao = Double.parseDouble(conteudo.get("imDbRating"));
            // int numeroEstrelas = (int) classificacao;
            // for (int n = 1; n <= numeroEstrelas; n++) {
            //     if (numeroEstrelas > 6) {
            //         System.out.print("⭐");
            //     } else {
            //         System.out.print("🍅");
            //     }
            // }
            // System.out.println("\n");

            //InputStream inputStream = new URL(conteudo.get("image")).openStream();
            //InputStream inputStreamFile = new FileInputStream(new File("entrada/conteudo.jpg"));
            //geradora.cria(inputStream, "saida/"+conteudo.get("title")+".png", "Filmão Hein!!!");
        //}

    }
}
