import java.util.List;
import java.util.Map;

public class TopMovies {

    private String url;
    private String imbdApiKey;
    private List<Map<String, String>> listadeFilmes;

    public TopMovies(String imdbApiKey) {
        this.url = "https://raw.githubusercontent.com/alura-cursos/imersao-java-2-api/main/TopMovies.json?key="
                + imdbApiKey;
    }

    public List<Map<String, String>> obterLista() throws Exception {
        // consumir a api e buscar os TOP movies;
        this.listadeFilmes = ApiConsumer.consumeSimpleGet(url);
        return this.listadeFilmes;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImbdApiKey() {
        return imbdApiKey;
    }

    public void setImbdApiKey(String imbdApiKey) {
        this.imbdApiKey = imbdApiKey;
    }

    public List<Map<String, String>> getListadeFilmes() {
        return listadeFilmes;
    }

    public void setListadeFilmes(List<Map<String, String>> listadeFilmes) {
        this.listadeFilmes = listadeFilmes;
    }

}
