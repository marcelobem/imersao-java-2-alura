import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.List;
import java.util.Map;

public class ApiConsumer {

    private static JsonParser parser = new JsonParser();

        public static List<Map<String, String>> consumeSimpleGet(String url) {

        try {
            HttpClient cliente = HttpClient.newHttpClient();
            HttpRequest req = HttpRequest.newBuilder(URI.create(url)).GET().build();
            HttpResponse<String> resposta = cliente.send(req, BodyHandlers.ofString());
            String body = resposta.body();
            return parser.parse(body);
        } catch (Exception e) {
            // TODO: handle exception, for now return a empty List
            return parser.parse("");
        }

    }

    public static List<Map<String, String>> consumerPostWithBearer(String givenUrl, String token, String jsonInputString) throws Exception {
        URL url = new URL(givenUrl);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Accept", "application/json");
        con.setRequestProperty("Authorization", "Bearer " + token);
        con.setDoOutput(true);

        try (OutputStream os = con.getOutputStream()) {
            byte[] input = jsonInputString.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(con.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }

            String body = response.toString();
            return parser.parse(body);
        }

    }

}
